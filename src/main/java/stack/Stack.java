package stack;

import java.util.Arrays;

//Implements Last In First Out List
public class Stack<E> {
    private int INITIAL_CAPACITY = 10;

    Object [] objects = new Object[INITIAL_CAPACITY];
    private int size = 0;

    //Doubles the size of the array if full and copy the original method.
    private void increaseCapacity(){
        if(size == objects.length)
        {
            objects = Arrays.copyOf(objects, objects.length*2);
        }
       // System.out.println(objects.length);
    }

    //Creates an Empty Stack, No argument constructor
    public void Stack(){
    }

    //Adds an element after the Last element
    public void push(Object object){
        increaseCapacity();
        objects[size] = object;
        size++;
    }

    //Removes the last added element and reduces the size by one.
    public Object pop(){
        Object pop = objects[size-1];
        objects[size-1] = null;
        size--;
        return pop;
    }

    //Returns the last added element
    public Object peek(){
        return objects[size-1];
    }
    //Checks that the Stack contains no element and size is zero, because size in
    //confidently well implemented, a check for size is enough to confirm emptiness
    public boolean isEmpty(){
        return size == 0;

    }




    //Returns the number of element added to the stack, (Last index plus 1)
    public int size(){
        return size;
    }

}
