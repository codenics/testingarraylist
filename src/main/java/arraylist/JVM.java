package arraylist;


import com.sun.xml.internal.ws.api.ha.StickyFeature;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class JVM {

    public static int[] intArray() {
        int[] fishes = {2, 3, 4,};
        return fishes;
    }
    public static void main (String [] args){
        ArrayListB arrayListB = new ArrayListB();

       arrayListB.add(Languages.CLOJURE);
       arrayListB.add(Languages.JAVA);
       arrayListB.add(Languages.PROCESSING);

        System.out.printf("This is %.30f\n" , Math.PI);
        System.out.println(7 + 8 + 8.0);
       // System.out.println((char) (6 +'c'));

        String str = null;
        System.out.println(str == null);

        arrayListB.addAll(Arrays.asList(3, 4, 5));

        System.out.println(arrayListB.size());
        System.out.println(arrayListB.get(1));
    }


//    public static void printArray(@NotNull Object[] list) {
//
//        for (Object o : list
//        ) {
//            System.out.println(o + " ");
//        }
//    }

    public static double interpret (){
        Stack<String> operators = new Stack<>();
        Stack<String> operands = new Stack<>();

        System.out.print("Please type the expresion to be computed: ");
        Scanner input = new Scanner(System.in);
        String [] expresion = input.nextLine().split("");


        for (String str :
             expresion) {
            if(str.equals("(")) System.out.println("Ignored (");
            else if (str.equals("/")) operators.push(str);
            else if (str.equals("*")) operators.push(str);
            else if (str.equals("-")) operators.push(str);
            else if (str.equals("+")) operators.push(str);
            else if (str.equals(")")) {
                String operator = operators.pop();
                String operand = operands.pop();

                Double value  = new Double(0);
                if(operator.equals("/")) {
                    value = Double.parseDouble(operand) / Double.parseDouble(operands.pop());
                    operands.push(String.valueOf(value));
                }
                else if (operator.equals("*")) {
                    value = Double.parseDouble(operand) * Double.parseDouble(operands.pop());
                    operands.push(String.valueOf(value));
                }
                else if (operator.equals("+")) {
                    value = Double.parseDouble(operand) + Double.parseDouble(operands.pop());
                    operands.push(String.valueOf(value));
                }
                else if (operator.equals("-")) {
                    value = Double.parseDouble(operand) - Double.parseDouble(operands.pop());
                    operands.push(String.valueOf(value));
            }
        }else operands.push(str);
        }

        return Double.parseDouble(operands.pop());
    }

}
