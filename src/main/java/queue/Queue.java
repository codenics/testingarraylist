package queue;


import java.util.Arrays;

//Implements First In First Out List

public class Queue<E> {
        private int INITIAL_CAPACITY = 10;

        Object [] objects = new Object[INITIAL_CAPACITY];
        private int size = 0;

        //Doubles the size of the array if full and copy the original method.
        private void increaseCapacity(){
            if(size == objects.length)
            {
                objects = Arrays.copyOf(objects, objects.length * 2);
            }
            // System.out.println(objects.length);
        }

        //Creates an Empty Queue, No argument constructor
        public void Queue(){
        }

        //Adds an element after the Last element
        public void enqueue(Object object){
            increaseCapacity();
            objects[size] = object;
            size++;
        }

        //Removes the first added element and reduces the size by one.
        public Object dequeue(){

            Object dequeue = objects[0];
            for (int i = 0; i < size ; i++) {
                objects[0] = objects[i+1];
            }
            size--;
            return dequeue;
        }

        //Returns the last added element
        public Object peek(){
            return objects[0];
        }

        //Checks that the Queue contains no element and size is zero, because size in
        //confidently well implemented, a check for size is enough to confirm emptiness
        public boolean isEmpty(){
            return size == 0;

        }

        //Returns the number of element added to the Queue, (Last index plus 1)
        public int size(){
            return size;
        }
    }

