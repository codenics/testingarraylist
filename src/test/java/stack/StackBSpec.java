package stack;

import com.sun.xml.internal.ws.api.pipe.ServerTubeAssemblerContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class StackBSpec {
    Stack<String> stack;

    @BeforeEach
    void init(){
        stack = new Stack();
    }

    @Test
    @DisplayName("Element exist after push.")
    public void push(){
        stack.push("Nice Hat");
        assertEquals(1, stack.size());
    }

    @Test
    @DisplayName("Returns the right peek element or the Last Element added.")
    public void peek(){
        stack.push("Nice");
        assertEquals("Nice", stack.peek());
    }

    @Test
    @DisplayName("Removes the recently added item.")
    public void pop(){
        stack.push("Greatness");
        assertEquals("Greatness", stack.pop());
        stack.push("Beauty");
        assertEquals("Beauty", stack.pop());
        assertEquals(0, stack.size(), "Size must be zero");
    }

    @Test
    @DisplayName("Increase Capacity when elements more than the initial Capacity is added")
    public void increaseCapacity(){
        stack.push("JANUARY");
        stack.push("FEBRUARY");
        stack.push("MARCH");
        stack.push("APRIL");
        stack.push("MAY");
        stack.push("JUNE");
        stack.push("JULY");
        stack.push("AUGUST");
        stack.push("SEPTEMBER");

        assertEquals(9, stack.size(), "Size must be 9");

        stack.push("OCTOBER");
        stack.push("NOVEMBER");
        stack.push("DECEMBER");

        assertEquals(12, stack.size(), "Size must be 12 now.");
    }

    @Test
    @DisplayName("Empty stact")
    public void isEmpty(){
        assertTrue(stack.isEmpty(), "initial stack must be empty");
        stack.push("Brown");
        stack.pop();
        assertTrue(stack.isEmpty(), "Stack must be empty after push and pop.");
    }
}
