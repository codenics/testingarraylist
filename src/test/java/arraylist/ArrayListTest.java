package arraylist;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

public class ArrayListTest
{
	////////////////////////////////////////////////////////
	// Constructor
	
	@Test
	void constructor()
	{
		final List<?> list = new ArrayListB();
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());
	}

	////////////////////////////////////////////////////////
	// get
	@Test
	void getFromEmpty()
	{
		final List<?> list = new ArrayListB();
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(0));
	}

	@Test
	void getOutOfRange()
	{
		final List list = new ArrayListB();
		list.add("Foo");
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(10));
	}
	
	@Test
	void get()
	{
		final List list = new ArrayListB();
		
		final Integer i1 = 1;
		final Integer i2 = 1;
		final Integer i3 = 1;

		list.add(i1); 
		assertEquals(i1, list.get(0));

		list.add(i2); 
		assertEquals(i1, list.get(0));
		assertEquals(i2, list.get(1));

		list.add(i3); 
		assertEquals(i1, list.get(0));
		assertEquals(i2, list.get(1));
		assertEquals(i3, list.get(2));

		assertFalse(list.isEmpty());
		assertEquals(3, list.size());
	}
	
	////////////////////////////////////////////////////////
	// add
	@Test
	void addToEmpty()
	{
		final List list = new ArrayListB();
		list.add("Foo");
		
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());
		assertEquals("Foo", list.get(0));
	}

	@Test
	void addWithGrowth()
	{
		final List list = new ArrayListB();
		
		for (int i = 0; i < 100; i++)
		{
			list.add(i);
			assertFalse(list.isEmpty());
			assertEquals(i + 1, list.size());
			assertEquals(i, list.get(i));
			
			// verify full state
			for (int j = 0; j <= i; j++)
			{
				assertEquals(j, list.get(j));
			}
		}
	}

	////////////////////////////////////////////////////////
	// add(int, Object)
	@Test
	void addAtBeginning()
	{
		final List list = new ArrayListB();
		list.add(0, 0);
		list.add(0, 1);
		list.add(0, 2);
		
		assertFalse(list.isEmpty());
		assertEquals(3, list.size());
		
		assertEquals(0, list.get(2));
		assertEquals(1, list.get(1));
		assertEquals(2, list.get(0));
	}

	@Test
	void addAtTheEnd()
	{
		final List list = new ArrayListB();
		list.add(0, 0);
		list.add(1, 1);
		list.add(2, 2);
		
		assertFalse(list.isEmpty());
		assertEquals(3, list.size());
		
		assertEquals(0, list.get(0));
		assertEquals(1, list.get(1));
		assertEquals(2, list.get(2));
	}

	@Test
	void addInTheMiddle()
	{
		final List list = new ArrayListB();
		list.add(0); // 0
		list.add(10); // 0, 10
		
		list.add(1, 9); // 0, 9, 10
		list.add(1, 8); // 0, 8, 9, 10
		list.add(2, 7); // 0, 8, 7, 9, 10
		list.add(2, 6); // 0, 8, 6, 7, 9, 10
		
		assertEquals(6, list.size());
		
		assertEquals(0, list.get(0));
		assertEquals(8, list.get(1));
		assertEquals(6, list.get(2));
		assertEquals(7, list.get(3));
		assertEquals(9, list.get(4));
		assertEquals(10, list.get(5));
	}

	@Test
	void addSame()
	{
		Object o = new Object();
		
		final List list = new ArrayListB();
		list.add(o); 
		list.add(o); 
		list.add(o); 

		assertEquals(3, list.size());
		assertEquals(o, list.get(0));
		assertEquals(o, list.get(1));
		assertEquals(o, list.get(2));
	}
	
	////////////////////////////////////////////////////////
	// addAll
	@Test
	void addAllBothEmpty()
	{
		final List target = new ArrayListB();
		final List source = new ArrayListB();
		target.addAll(source);
		
		assertTrue(target.isEmpty());
		assertEquals(0, target.size());

		assertTrue(source.isEmpty());
		assertEquals(0, source.size());
	}
	
	@Test
	void addAllSourceEmpty()
	{
		final List target = new ArrayListB();
		target.add("t1");
		
		final List source = new ArrayListB();
		target.addAll(source);
		
		assertFalse(target.isEmpty());
		assertEquals(1, target.size());

		assertTrue(source.isEmpty());
		assertEquals(0, source.size());
	}
	
	@Test
	void addAllTargetEmpty()
	{
		// Final keywords ones created cannot be modified
		// Final keywords ones created cannot be modified

		List target = new ArrayListB();
		List source = new ArrayListB();

		//
		source.add("s1");
		target.addAll(source);
		
//		assertFalse(target.isEmpty());
//		assertEquals(1, target.size());
//
//		assertEquals("s1", target.get(0));
//
//		assertFalse(source.isEmpty());
//		assertEquals(1, source.size());
	}
	
	@Test
	void addAll()
	{
		final List target = new ArrayListB();
		target.add("t1");

		
		final List source = new ArrayListB();
		source.add("s1");
		
		target.addAll(source);
		
		assertFalse(target.isEmpty());
		assertEquals(2, target.size());

		assertEquals("t1", target.get(0));
		assertEquals("s1", target.get(1));
		
		assertFalse(source.isEmpty());
		assertEquals(1, source.size());
	}
	
	
	////////////////////////////////////////////////////////
	// clear
	@Test
	void clearEmpty()
	{
		final List list = new ArrayListB();
		list.clear();
		
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());
	}
	
	@Test
	void clearAndFill()
	{
		final List list = new ArrayListB();
		list.add("1");
		list.add("2");
		list.clear();
		
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());

		list.add("3");
		list.add("4");
		assertEquals(2, list.size());
		assertEquals("3", list.get(0));
		assertEquals("4", list.get(1));
	}

	////////////////////////////////////////////////////////
	// contains
	@Test
	void containsEmpty()
	{
		final List list = new ArrayListB();
		assertFalse(list.contains("foo"));
	}

	@Test
	void containsNotIn()
	{
		final List list = new ArrayListB();
		list.add("bar");
		list.add("bar2");
		
		assertFalse(list.contains("foo"));
	}

	@Test
	void containsInTheList()
	{
		final List list = new ArrayListB();
		list.add("bar1");
		list.add("bar2");
		list.add("bar3");
		
		assertTrue(list.contains("bar2"));
		assertTrue(list.contains("bar3"));
		assertTrue(list.contains("bar1"));
	}

	
	////////////////////////////////////////////////////////
	// remove
	@Test
	void removeFromEmpty()
	{
	}

	@Test
	void removeNonInList()
	{
	}

	@Test
	void removeFromFirst()
	{
	}

	@Test
	void removeFromLast()
	{
	}

	@Test
	void removeFromMiddle()
	{
	}

	@Test
	void removeLast()
	{
	}

	@Test
	void removeEqualsButNotSame()
	{
	}

}
