package arraylist;

/*
    @Darko Gibert
    Xceptance Software Technologies
    07741 Jena

   **A Junit 5 Testing suite for the ArrayList Class in in java.util package**
 */

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayListBSpec {
    private List JVMLang;

    @BeforeEach
    void init() {
        JVMLang = new ArrayListB<Languages>();
    }

    @Test
    @DisplayName("Testing the arrayList constructor with maximum integer value.")
    public void arrayListConstructorTestMax() {
        assertThrows(OutOfMemoryError.class, () -> JVMLang = new ArrayList<>(Integer.MAX_VALUE),
                "Should throw out of memory error.");
    }

    @Test
    @DisplayName("Testing the arrayList constructor with maximum integer value.")
    public void arrayListConstructorTestMin() {
        assertThrows(IllegalArgumentException.class, () -> new ArrayList<>(Integer.MIN_VALUE), "" +
                "Should throw illegalArgumentException with for a negative integer.");
    }

    @Test
    @DisplayName("Testing the arrayList constructor with negative integer values.")
    public void arrayListConstructorTestNegative() {
        assertThrows(IllegalArgumentException.class, () -> new ArrayList<>(-1));
        assertThrows(IllegalArgumentException.class, () -> new ArrayList<>(-2000000000));
    }

    @Test
    @DisplayName("Testing the arrayList constructor with zero")
    public void arrayListConstructorTestZero() {
        assertEquals(0, new ArrayList(00).size(),
                "Empty arrayList has a size of 0");
    }

    @Test
    @DisplayName("Size should be equal to total number of elements.")
    public void size() {
        ArrayList<Languages> arrays = new ArrayList<>();
        assertEquals(0, arrays.size());
    }

    @Test
    @DisplayName("ADD - index and values.")
    public void sizeAfterAdd() {
        JVMLang.add(Languages.CPP);
        // ((ArrayList)JVMLang).trimToSize();
        assertEquals(1, JVMLang.size(), "Size must be 1.");
        assertEquals(Languages.CPP, JVMLang.get(0), " Should print the current Value - CPP.");

        JVMLang.add(Languages.KAWA);//Add one more element to make 2
        assertEquals(2, JVMLang.size(), "Size must increase to 2.");
        assertEquals(Languages.CPP, JVMLang.get(0), "First added element must still be there.");
        assertEquals(Languages.KAWA, JVMLang.get(1), "So is the subsequent added elements.");

        JVMLang.add(Languages.CPP);
        JVMLang.add(Languages.SCALA);
        JVMLang.add(Languages.KAWA);
        JVMLang.add(Languages.KOTLIN);
        JVMLang.add(Languages.PROCESSING);
        assertEquals(7, JVMLang.size());
        assertEquals(Languages.PROCESSING, JVMLang.get(6));

    }

    @Test
    @DisplayName("Contains Added Index")
    public void containsAddedElement() {
        JVMLang.add(Languages.CLOJURE);
        assertEquals(Languages.CLOJURE, (JVMLang.get(0)));
        JVMLang.add(Languages.XTEND);
        assertEquals(Languages.XTEND, JVMLang.get(1));
    }


    @Test
    @DisplayName("Elements added at a particular index should be at that index.")
    public void addAtIndex() {
        JVMLang.add(Languages.CLOJURE);
        JVMLang.add(Languages.FANTOM);
        JVMLang.add(1, Languages.XTEND);

        assertEquals(Languages.XTEND, JVMLang.get(1));
        assertEquals(Languages.CLOJURE, JVMLang.get(0));

        JVMLang.add(Languages.GROOVY);//4
        JVMLang.add(Languages.JAVA);//5
        JVMLang.add(Languages.JYTHON);//6
        JVMLang.add(Languages.SCALA);//7
        JVMLang.add(Languages.PROCESSING);//8
        //JVMLang.add(3, Languages.KAWA);
        //assertEquals(Languages.PROCESSING, JVMLang.get());

    }

    @Test
    @DisplayName("In Java Null is an Object, and the add implementation can decide to add or not.")
    public void addNull() {
        JVMLang.add(null);
        assertEquals(1, JVMLang.size(), "null is considered a value(Object) must be added");
    }

    @Test
    public void addMultiNull() {
        for (int i = 0; i < 100; i++) {
            JVMLang.add(null);
        }
        assertEquals(100, JVMLang.size());
    }

    @Test
    @DisplayName("Adds null at a specified index.")
    public void addNullIndex() {
        JVMLang.add(null);
        JVMLang.add(0, null);
        JVMLang.add(0, Languages.CPP);
        assertEquals(null, JVMLang.get(1), "null is shifted one step, after an object is inserted.");
        assertEquals(Languages.CPP, JVMLang.get(0), "add object should be at the former index of null.");
    }

    @Test
    public void emptyList() {
        assertTrue(JVMLang.isEmpty(), "List now must be empty.");
        JVMLang.add(Languages.SCALA);
        JVMLang.add(Languages.CLOJURE);
        assertFalse(JVMLang.isEmpty(), "List not anymore empty after adding elements.");
    }

    @Test
    public void containsElements() {
        JVMLang.add(Languages.CPP);
        assertTrue(JVMLang.contains(Languages.CPP));
        assertEquals(Languages.CPP, JVMLang.get(0), "The value must be CPP.");
    }

    @Test
    @DisplayName("ADDALLCollections - Indexes and Values")
    public void addAll() {

        assertTrue(JVMLang.addAll(Arrays.asList(Languages.values())), "True if collection was added Successfully.");
        assertEquals(11, JVMLang.size());
        assertEquals(Languages.values().length, JVMLang.size(), "Length of Enum and Array size must be equal.");
        assertTrue(JVMLang.contains(Languages.SCALA), "Contains a particular element after addAll");

        //Implement addAll @ an index,
    }
    @Test
    public void addAllEmpty(){
        List pool = new ArrayListB();
        List fishes = new ArrayListB();

        fishes.add("j");
        pool.addAll(fishes);
//
//        List target = new ArrayListB();
//        List source = new ArrayListB();
//
//        source.add("s1");
//        target.addAll(source);
    }

    @Test
    @DisplayName("Cleared List Should BeEmpty")
    public void clear() {
        JVMLang.add(Languages.CPP);
        JVMLang.add(Languages.GROOVY);
        assertFalse(JVMLang.isEmpty());

        JVMLang.clear();
        assertTrue(JVMLang.isEmpty());
    }

    @Test
    @DisplayName("Test equality of the ArrayList implementation.")
    public void equality() {
        assertTrue(JVMLang.equals(JVMLang), "Empty lists are equal.");
        List<Languages> JVMLang2 = new ArrayListB();

        JVMLang2.add(Languages.CPP);
        assertFalse(JVMLang.equals(JVMLang2), "List must have the same number of element to be equal.");

        JVMLang.add(Languages.GROOVY);
        assertFalse(JVMLang.equals(JVMLang2), "List must have the same element at same Location.");

        JVMLang2.add(Languages.GROOVY);
        JVMLang.add(Languages.CPP);
        assertFalse(JVMLang.equals(JVMLang2), "List must have the same element at same Location.");

        JVMLang.clear();
        JVMLang2.clear();

        JVMLang.addAll(Arrays.asList(Languages.values()));
        JVMLang2.addAll(Arrays.asList(Languages.values()));

        assertTrue(JVMLang2.equals(JVMLang2), "Same size, Same elements, elements at same position.");
        assertTrue(JVMLang2.hashCode() == JVMLang2.hashCode(), "Hash code must be same.");

        List<Languages> list1 = new ArrayListB();
        List<Languages> list2 = list1;
        List<Languages> list3 = list2;


        assertTrue(list1.equals(list1), "REFLECTIVE - equal to ITSELF.");
        assertTrue(list1.equals(list2), "SYMMETRY - Equal to YOU.");
        assertTrue(list2.equals(list1), "SYMMETRY - Equal to ME.");
        assertTrue(list1.equals(list3), "TRANSITIVE - Equals of equals are equal.");
        assertTrue(list2.equals(list3), "TRANSITIVE - Equals of equals are equal.");

    }

    @Test
    @DisplayName("Index of the last appearance of an element.")
    public void lastIndexOf() {
        JVMLang.addAll(Arrays.asList(Languages.values()));
        assertEquals(JVMLang.lastIndexOf(Languages.KAWA), 10);
        JVMLang.add(Languages.CLOJURE);
        JVMLang.add(Languages.FANTOM);
        JVMLang.add(Languages.CLOJURE);
        assertEquals(13, JVMLang.lastIndexOf(Languages.CLOJURE));

    }

    @Test
    @DisplayName("Remove at a specified position.")
    public void removeAtPosition() {
        JVMLang.add(Languages.GROOVY);
        JVMLang.add(Languages.KOTLIN);
        assertEquals(Languages.KOTLIN, JVMLang.remove(1), "Must return element at specified position.");
        assertEquals(1, JVMLang.size(), "Size must reduce after removing");
    }

    @Test
    @DisplayName("Remove Object")
    public void removeObject() {
        JVMLang.add(Languages.FANTOM);
        JVMLang.add(Languages.XTEND);
        assertFalse(JVMLang.remove(Languages.PROCESSING), "Prints element not found in the list, according to the implementation.");
        assertTrue(JVMLang.remove(Languages.FANTOM), "Return true if element exit and got removed.");

        assertEquals(1, JVMLang.size(), "Size should decrease by no. of elements removed.");
        assertFalse(JVMLang.contains(Languages.FANTOM), "No more contains element");

    }

}
