package queue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class QueueBSpec {

        Queue<String> queue;

        @BeforeEach
        void init(){
            queue = new Queue();
        }

    @Test
    @DisplayName("Empty Queue")
    public void isEmpty(){
        assertTrue(queue.isEmpty(), "initial queue must be empty");
        queue.enqueue("Brown");
        queue.dequeue();
        assertTrue(queue.isEmpty(), "Queue must be empty after push and pop.");
    }

        @Test
        @DisplayName("Element exist after enqueuing.")
        public void enqueue(){
            queue.enqueue("Nice Hat");
            assertEquals(1, queue.size());
        }

        @Test
        @DisplayName("Returns the right peek element or the Last Element added.")
        public void peek(){
            queue.enqueue("Nice");
            assertEquals("Nice", queue.peek());
            queue.enqueue("Nicer");
            assertEquals("Nice", queue.peek());
        }

        @Test
        @DisplayName("Removes the first added item.")
        public void dequeue(){
            queue.enqueue("Greatness");
            assertEquals("Greatness", queue.dequeue(), "Returns the element dequeued.");
            assertEquals(0, queue.size(), "Size must be zero");
        }

        @Test
        @DisplayName("Removes the first added elements and shift the rest.")
        public void dequeueMoreElements(){
            queue.enqueue("Summer");
            queue.enqueue("Winter");
            queue.enqueue("Autumn");
            queue.enqueue("Fall");

            assertEquals("Summer", queue.dequeue());
            assertEquals(3, queue.size());
        }

        @Test
        @DisplayName("Increase Capacity when elements more than the initial Capacity are added")
        public void increaseCapacity(){
            queue.enqueue("JANUARY");
            queue.enqueue("FEBRUARY");
            queue.enqueue("MARCH");
            queue.enqueue("APRIL");
            queue.enqueue("MAY");
            queue.enqueue("JUNE");
            queue.enqueue("JULY");
            queue.enqueue("AUGUST");
            queue.enqueue("SEPTEMBER");

            assertEquals(9, queue.size(), "Size must be 9");

            queue.enqueue("OCTOBER");
            queue.enqueue("NOVEMBER");
            queue.enqueue("DECEMBER");

            assertEquals(12, queue.size(), "Size must be 12 now.");
        }

    }

