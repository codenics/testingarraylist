In my introduction to Test Driven Development @ Xceptance, I was asked to setup 
my environment and learn Junit 5.
The subsequent task was to test the methods in the Arraylist in Java.util
"Use List<> list = new ArrayList()" to create ArrayList object.
This project includes valid test cases for the methods in the Arraylist class.
In my next project I will implement my own arraylist and test it against these
tests in this Project. 
:)